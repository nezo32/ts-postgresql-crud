import express from "express";
import { pool } from "./queries.js";
import bodyParser from "body-parser";

const server = express();

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }))

// read
server.get('/', (req, res) => {
    pool.query('SELECT * FROM users ORDER BY id ASC', (err, result) => {
        if (err) res.status(500).json(err);
        else res.status(200).json(result.rows);
    })
})

// read by id
server.get('/users/:id', (req, res) => {
    const id = req.params.id
    pool.query('SELECT * FROM users WHERE id = $1', [id], (err, result) => {
        if (err) res.status(500).json(err);
        else res.status(200).json(result.rows);
    })
})

// create
server.post('/users', (req, res) => {
    const { name, email } = req.body;
    pool.query('INSERT INTO users (name, email) VALUES ($1, $2) RETURNING *', [name, email], (err, result) => {
        if (err) res.status(500).json(err);
        else res.status(200).json(`User added with ID: ${result.rows[0].id}`);
    })
})

// update
server.put('/users/:id', (req, res) => {
    const { name, email } = req.body;
    const id = req.params.id;
    pool.query('UPDATE users SET name = $1, email = $2 WHERE id = $3', [name, email, id], (err, result) => {
        if (err) res.status(500).json(err);
        res.status(200).json(`User modified with ID: ${id}`);
    })
})

// delete
server.delete('/users/:id', (req, res) => {
    const id = req.params.id
    pool.query('DELETE FROM users WHERE id = $1', [id], (err, result) => {
        if (err) res.status(500).json(err);
        else res.status(200).json(`User deleted with ID: ${id}`);
    })
})

server.listen(3000, () => {
    console.log('server started on 3000 port')
})
