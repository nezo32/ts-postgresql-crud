CREATE TABLE users (
    ID SERIAL PRIMARY KEY,
    name VARCHAR(30),
    email VARCHAR(30)
);

INSERT INTO users (name, email)
    VALUES ('Jerry', 'jerry@example.com'), ('George', 'george@example.com'), ('Nezo', 'nezo.prod32@mail.ru'), ('Sosok', 'bebra.stalin@aboba.cum');