FROM node:latest
WORKDIR /app
COPY ./server .
RUN yarn install
RUN yarn build
ENTRYPOINT [ "yarn", "start" ]